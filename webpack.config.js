const path = require('path');
const glob = require('glob');

module.exports = {
    entry: getEntries(),
    mode: 'production',
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react'],
            },
          },
        },
        {
          test: /\.css$/,
          use: ['css-loader'],
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
  };


  function getEntries() {
    const entryFiles = glob.sync(path.resolve(__dirname, 'src/*.jsx'));
  
    const entries = {};
    entryFiles.forEach((entry) => {
      const name = path.basename(entry, '.jsx');
      entries[name] = entry;
    });
  
    return entries;
  }